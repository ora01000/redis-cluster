package ora01000.redis;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

public class RedisHandler {
	
	public static RedisHandler singleton = null;
	public static Set<HostAndPort> JEDIS_CLUSTER_NODES = new HashSet<HostAndPort>();
	public static JedisCluster JEDIS_CLUSTER = null;
	
	static {
		String[] ips = new String[] {"172.30.147.36", "172.30.210.106", "172.30.206.137", "172.30.92.116"};
		int[] port = new int[] {6379, 6379, 6379, 6379};
		
		for(int i = 0; i < ips.length; i++)
		{
			JEDIS_CLUSTER_NODES.add(new HostAndPort(ips[i], port[i]));
			System.out.println("=======>" + ips[i] + ":" + port[i]);
		}
		JEDIS_CLUSTER = new JedisCluster(JEDIS_CLUSTER_NODES);
	}
	
	public static RedisHandler getInstance()
	{
		if(singleton == null)
		{
			singleton = new RedisHandler();
		}
		return singleton;
	}

	
	public void generateRandomValues(int count)
	{
		Random rand = new Random(new Date().getTime());
		//byte[] array = new byte[7]; // length is bounded by 7
		
		for(int k = 0; k < count; k++)
		{
			String value = rand.ints(48, 122 + 1)
				      .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
				      .limit(18)
				      .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
				      .toString();
			
			String key = String.valueOf(k);
		
			JEDIS_CLUSTER.set(key, value);
		}
		
	}
	
	public void getData(int count)
	{
		HashMap<String, String> list = new HashMap<String, String>();
		for(int i = 0; i < count; i++)
		{
			list.put(String.valueOf(i), JEDIS_CLUSTER.get(String.valueOf(i)));
		}
		
		Set<String> set = list.keySet();
		Iterator<String> iter = set.iterator();
		while(iter.hasNext())
		{
			String key = iter.next();
			System.out.println("=======>>[key]:" + key + " [value]:" + list.get(key));
		}
		
		System.out.println("=========>> Total Count : " + set.size());
		
	}
	
	public void setNull(int count)
	{
		for(int k = 0; k < count; k++)
		{
			
			String key = String.valueOf(k);
		
			JEDIS_CLUSTER.set(key, "");
		}
	}

}
